<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript"
	src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript"
	src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
* {
	margin: 0px;
	padding: 0px;
}

body {
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
	font-family: 'Helvetica';
}

i.k-icon {
	padding: 10px;
	border: 1px solid black;
	border-radius: 5px;
	background: lightgray;
	font-size: 20px;
	width: 100px;
	color: black;
	-webkit-transition-duration: 0.4s; /* Safari */
	transition-duration: 0.4s;
}

i.k-icon:hover {
	background: darkgray;
	color: white;
}

.k-dialog.k-alert .k-dialog-titlebar {
	display: none;
}
.k-grid-header th.k-header{
	background: linear-gradient( to top, black, gray);
    color: white;
}
.k-grid-header th.k-header>.k-link{
	color: white;
}
td.k-group-cell{
	background: gray;
}
#datePicker{
    background : rgb(121, 218, 76);
    border-radius: 5px;
    font-weight: bolder;
}
</style>
<script type="text/javascript">
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	$(function(){
		createNav("kpi_nav", 4);
		
		setEl();
		setDate();	
		time();
		
		setEvt();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		getAttentList();
	});
	
	function setEvt(){

	};
	
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("input").css({
			"font-size" : getElSize(50),
			"height" : getElSize(100),
			"margin" : "0px",
		    "margin-right" : "0px",
		    "margin-left" : "0px",
		    "width" : getElSize(700)
		})
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#tableDiv").css({
			"width" : "100%",
			"margin-top" : getElSize(10),
			"background-color" : "black"
		});
		
		$("#attandant_grid").css({
			"width" : "100%",
			"margin-top" : getElSize(10),
			"background-color" : "black"
		});
		
		$("#select_worker_combo").css({
			"width" : getElSize(500),
			"font-size" : getElSize(40)
		})
		
		$("#sDate_attandant, #eDate_attandant").css({
			"width" : getElSize(300)
		})
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
</script>

<script>

var workerList=[];


	
function getWorkerWatingTime(){
	
	var dataSource = new kendo.data.DataSource({});
	$.showLoading()
	var url = "${ctxPath}/order/getWorkerCnt.do";
	console.log(workerCd)
	
	var sDate = moment($("#sDate").val());
	var eDate = moment($("#eDate").val());
	var diff = eDate.diff(sDate, 'days');
	
	if(diff<0){
	    kendo.alert("검색 범위 시작일자가 끝일자보다 작습니다. 시작일자를 변경하여 주십시오.")
	    $.hideLoading();
		return;
	}
	
	var dayList = [];
	for(var i=1;i<=diff;i++){
		sDate = moment($("#sDate").val());
		var Date=sDate.add(i,'days').format("YYYY-MM-DD");
		dayList.push(Date);
	}
	var param = "sDate="+$("#sDate").val()+
				"&eDate="+$("#eDate").val()+
				"&workIdx=0" + 
				"&prdNoList="+
				"&empCdList="+multiselect.value()
	var workerArray=[];
	$.ajax({
		url : url,
		dataType : "json",
		data : param,
		type : "post",
		success : function(data){
			var json = data.dataList;
			console.log(data)
			$(json).each(function(idx, data){
				
				if(data.oprCd=="0010"){
					data.oprNo = "R삭";
				}else if(data.oprCd=="0020"){
					data.oprNo = "MCT 1차";
				}else if(data.oprCd=="0030"){
					data.oprNo = "CNC 2차";
				}
				
				if(data.nd==1){
					data.nd = "${night}"
				}else{
					data.nd = "${day}"
				};
				var oprNo;
				if(data.oprNm=="0010"){
					data.oprNo = "R삭";
				}else if(data.oprNm=="0020"){
					data.oprNo = "MCT 1차";
				}else if(data.oprNm=="0030"){
					data.oprNo = "CNC 2차";
				}
				data.name = decode(data.name)
				data.worker = decode(data.worker)
				data.diff_cnt = (Number(data.workerCnt) - Number(data.cnt))
				
				dataSource.add(data)
				
			});
			
			dataSource.group([{field:'worker', aggregates:[{ field: "prdNo", aggregate: "count" } ] },{field:'date',  aggregates:[
			    { field: "goalRatio", aggregate: "average" },   
			    { field: "workerCnt", aggregate: "sum" },   
			    { field: "worker", aggregate: "max" },
			    {field:'average', aggregate: "max"}
			]}]);
			dataSource.aggregate([
			    { field: "goalRatio", aggregate: "average" },
			  ]);
			dataSource.sort({ field: "prdNo", dir: "asc" });
			
			dataSource.fetch().then(function(){
				  var data = dataSource.data();
			});
			
			grid.setDataSource(dataSource);
			
			$.hideLoading()
		}
	});
}



function setDate(){
	var date = new Date();
	date.setDate(date.getDate()-2)
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var sMonth = addZero(String(date.getMonth()));
	var day = addZero(String(date.getDate()));
	
	$("#sDate_attandant").val(year + "-" + sMonth + "-" + day);
	$("#eDate_attandant").val(year + "-" + month + "-" + day);
};


$(function(){
	
	$( "#sDate_attandant" ).datepicker({
		onSelect:function(){
		}
	});
	$( "#eDate_attandant" ).datepicker({
		onSelect:function(){
		}
	});
	
	setDate()
	
	var dataWorkerList_combo = $("#select_worker_combo").kendoComboBox({
		dataTextField:"name",
		dataValueField:"workerCd",
		noDataTemplate: '작업자를 발견하지 못하였습니다.',
		placeholder: "입력 해주세요",
		change : function(e){
			var value = this.value();
			console.log(value)
			workerCd = value;
		},select : function(e){
			var value = this.value();
			console.log(value)
			workerCd = value;
		}
		}).data("kendoComboBox");
	
	$.ajax({
		url : "${ctxPath}/common/getAllWorkerList.do",
		dataType : "json",
		type : "post",
		success : function(data){
			var ds = new kendo.data.DataSource(); 
			var json = data.dataList;
			var options = "";
			
			$(json).each(function(idx, data){
				var object = {
						"workerCd" : data.id
						,"name" : decode(data.name)
				}
				workerList.push(object)
				ds.add(object)
				
			});
			ds.fetch().then(function(){
				  var data = ds.data();
			});
			dataWorkerList_combo.setDataSource(ds)
		}, error: function(result) {
			
	        console.log(result);
	    }
	});
	
	attandant_grid = $("#attandant_grid").kendoGrid({
		  
		selectable: "row",
	    height:  getElSize(1450),
	    sortable: true,
	    editable: false,
	    scrollable: true,
	    filterable: false,
		columns: [
			{ field: "name", title:"작업자",
				attributes: {
    				style: "text-align: center; color:white; font-size:" + getElSize(35) +"; overflow: visible !important;"
    			},headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
    		},
	        { field: "group1", title:"근태유형",
    			attributes: {
    				style: "text-align: center; color:white; font-size:" + getElSize(35) +"; overflow: visible !important;"
    			},headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
    		},
	        { field: "group2", title:"근태분류",
    			attributes: {
    				style: "text-align: center; color:white; font-size:" + getElSize(35) +"; overflow: visible !important;"
    			},headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
    		},
	        { field: "group3", title:"근태",
    			attributes: {
    				style: "text-align: center; color:white; font-size:" + getElSize(35) +"; overflow: visible !important;"
    			},headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
    		},
	        { field: "date", title:"일자",
    			attributes: {
    				style: "text-align: center; color:white; font-size:" + getElSize(35) +"; overflow: visible !important;"
    			},headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
    		}
			]
	    
	}).data("kendoGrid");
	
})

var workerCd;

function getAttentList(){
	$.showLoading()
	if(typeof workerCd=='undefined'){
		workerCd="0";
	}
	
	
	$("#print_table").empty();
	
	var table ="<table class='p_table' border='5' width='95%' align='center'><tr><th colspan='2' rowspan='3'>작업자 근태" +
							"<br>"+ $("#sDate_attandant").val() +'~' + $("#eDate_attandant").val() +"</th><th style='background : lightgray' colspan='3'>결제</th></tr>" +
							"<tr style='background : lightgray'><th>담당</th><th>검토</th><th>승인</th></tr>" +
							"<tr class='sign' ><th> </th><th> </th><th> </th></tr>" +
							"<tr style='background : lightgray'><th>작업자</th><th>근태유형</th><th>근태분류</th><th>근태</th><th>일자</th>" ;
	
	
	var attent_DataSource = new kendo.data.DataSource();
	var url = "${ctxPath}/chart/getAttendant_List.do";
	var param = "empCd="+workerCd+"&sDate="+$("#sDate_attandant").val()+"&eDate="+$("#eDate_attandant").val()
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			console.log(json)
			$(json).each(function(idx, data){
				data.name=decode(data.name)
				data.group1=decode(data.group1)
				data.group2=decode(data.group2)
				data.group3=decode(data.group3)
				attent_DataSource.add(data)
			});
			
			attent_DataSource.fetch().then(function(){
				  var data = attent_DataSource.data();
			});
			
			attandant_grid.setDataSource(attent_DataSource);
			var dataItem=attandant_grid.dataSource.data();
			
			
			//달성률로 object 정렬
			dataItem.sort(function (a, b) {
				return b.name - a.name;
			});
			
			//print table 그리는 부분  (행:9)

			var chk=1;
			
			//프린트 그리는 총 테이블 갯수 구하기 chk
			$(dataItem).each(function(idx,data){
				chk++;
				if(idx!=0){
					if(dataItem[idx].name!=dataItem[idx-1].name){
						chk++;
					}
				}
			});
			//테이블 몇번째 열 까지 뽑을지 (A4사이즈 맞추기 위해서)
			var divideNum=36;		
			var page=1;		//page size표시
			var totalpage=Math.ceil(chk/divideNum);		//총 페이지수
			var index=0;	//행 갯수 세기
			var sameName=0;
			$(dataItem).each(function(idx,data){
				
					//한개의 tr
					if(idx==0){
						index++;
						table +="<tr style='background : lightgray' class='totalwork'><td colspan='5'> 작업자 : "+ data.name +"</td>";
					}else{	
						index++;
						if(dataItem[idx].worker!=dataItem[idx-1].worker){
							index++;
							table +="<tr style='background : lightgray' class='totalwork'><td colspan='5'> 작업자 : "+ data.name +"</td>";
						}
					}
					
					//tr
					table +="<tr><td>" + data.name + "</td><td>" +	data.group1 + "</td><td>" + data.group2 + "</td><td>" + data.group3 + "</td><td>" + data.date;
					
					//a4 size page 넘기기
					if(idx!=0 && index>divideNum ){	
						page++;
						index=1;
						table +="</table>" +
								"<div style='page-break-before:always'></div>";
								
						table +="<br><br><br><br> <table class='p_table' border='5' width='95%' align='center'><tr class='sign'><th colspan='7' rowspan='2'>생산완료입력 조회</th>	<th colspan='2'>페이지</th></tr>" +
								"<tr class='sign'><th colspan='2'>" + page +" / " + totalpage +"</th></tr>" +
								"<tr style='background : lightgray'><th>작업자</th> <th>근태유형</th> <th>근태분류</th> <th>근태</th> <th>일자</th>";
								
					}
			})
			
			table +="</table>"
			$("#print_table").append(table);
			
			//print css
			$(".sign").css({
				"height" : "50px"
			})
			$(".sign th").css({
				"width" : "70px"
			})

			$(".p_table tr th").css("font-size","11px")
			$(".p_table tr td").css("font-size","11px")
			$.hideLoading()
		}
	});
	
}
var multiselect;

function pop_print(){
		win = window.open();
        self.focus();
        win.document.open();
        win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
        win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
        win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
        win.document.write(document.getElementById('print_table').innerHTML);
        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
        win.document.close();
        win.print();
        win.close();
	}
	
$(function(){
	$("input#sDate_attandant").datepicker({
		"minDate": new Date(2017, 12-1, 1),
        "maxDate": 0,
		onSelect:function(){
		}
	})
	$("input#eDate_attandant").datepicker({
		"minDate": new Date(2017, 12-1, 1),
        "maxDate": 0,
		onSelect:function(){
		}
	})
	$(".day").css({
		"background" : "green",
		"color" : "white"
	})
})
	
	
	
	
	
var startDate,
endDate,
selectCurrentWeek = function () {
    window.setTimeout(function () {
        $('#sDate_attandant,#eDate_attandant').datepicker('widget').find('.ui-datepicker-current-day a').addClass('ui-state-active')
    }, 1);
};

function daily(){
	
	$( "input#sDate_attandant" ).datepicker( "destroy" );
	$( "input#eDate_attandant" ).datepicker( "destroy" );
	
	$('.ui-weekpicker').on('mousemove', 'tr', function () {
       $(this).find('td a').removeClass('ui-state-hover');
   });
	
	$("input#sDate_attandant").datepicker({
		onSelect:function(){
			//showLoading()
		}
	})
	$("input#eDate_attandant").datepicker({
		onSelect:function(){
			//showLoading()
		}
	}) 
	
	$("#dailyRange").css({
		"display" : "inline"
	})
	$("#weeklyRange").css({
		"display" : "none"
	})
	$("#monthlyRange").css({
		"display" : "none"
	})
	
	$(".day").css({
		"background" : "green",
		"color" : "white"
	})
	$(".week").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	}) 
	$(".month").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	})
	$("#actionBtn").css({
		"display":"inline"
	})
}

function weekly(){
	
	$( "input#sDate_attandant" ).datepicker( "destroy" );
	$( "input#eDate_attandant" ).datepicker( "destroy" );
	
	$('input#sDate_attandant').datepicker({
       "showOtherMonths": false,
       "selectOtherMonths": false,
       "onSelect": function (dateText, inst) {
           var date = $(this).datepicker('getDate'),
               dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
           startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
           endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
           $('#sDate_attandant').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
           $('#eDate_attandant').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
           selectCurrentWeek();
       },
       "beforeShow": function () {
           selectCurrentWeek();
       },
       "beforeShowDay": function (date) {
           var cssClass = '';
           if (date >= startDate && date <= endDate) {
               cssClass = 'ui-datepicker-current-day';
           }
           return [true, cssClass];
       },
       "onChangeMonthYear": function (year, month, inst) {
           selectCurrentWeek();
       }
   }).datepicker('widget').addClass('ui-weekpicker');
   
   $('input#eDate_attandant').datepicker({
       "showOtherMonths": false,
       "selectOtherMonths": false,
       "onSelect": function (dateText, inst) {
           var date = $(this).datepicker('getDate'),
               dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
           startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
           endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
           $('#sDate_attandant').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
           $('#eDate_attandant').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
           selectCurrentWeek();
       },
       "beforeShow": function () {
           selectCurrentWeek();
       },
       "beforeShowDay": function (date) {
           var cssClass = '';
           if (date >= startDate && date <= endDate) {
               cssClass = 'ui-datepicker-current-day';
           }
           return [true, cssClass];
       },
       "onChangeMonthYear": function (year, month, inst) {
           selectCurrentWeek();
       }
   }).datepicker('widget').addClass('ui-weekpicker');
   
   
   $('.ui-weekpicker').on('mousemove', 'tr', function () {
       $(this).find('td a').addClass('ui-state-hover');
   });
   $('.ui-weekpicker').on('mouseleave', 'tr', function () {
   	$(this).find('td a').removeClass('ui-state-hover');
   });
	
	$("#dailyRange").css({
		"display" : "inline"
	})
	$("#monthlyRange").css({
		"display" : "none"
	})
	$(".day").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	})
	$(".week").css({
		"background" : "green",
		"color" : "white"
	})
	$(".month").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	})
	$("#actionBtn").css({
		"display":"inline"
	})
}

function monthly(){
	$( "input#sDate_attandant" ).datepicker( "destroy" );
	$( "input#eDate_attandant" ).datepicker( "destroy" );
	
	$('input#sDate_attandant').datepicker( {
       changeMonth: true,
       changeYear: true,
       showButtonPanel: true,
       dateFormat: 'yy mm',
       onClose: function(dateText, inst) { 
           var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
           var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
           if(month<10){
           	month = '0'+month.toString()
           }
           
           $('input#sDate_attandant').val(year+'-'+month+'-'+'01');
           $('input#eDate_attandant').val(year+'-'+month+'-'+31);
       }
   });
	
	$('input#eDate_attandant').datepicker( {
       changeMonth: true,
       changeYear: true,
       showButtonPanel: true,
       dateFormat: 'yy mm',
       onClose: function(dateText, inst) { 
           var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
           var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
           if(month<10){
           	month = '0'+month.toString()
           }
           
           $('input#sDate_attandant').val(year+'-'+month+'-'+'01');
           $('input#eDate_attandant').val(year+'-'+month+'-'+31);
       }
   });
	
	$("#dailyRange").css({
		"display" : "inline"
	})
	
	$(".day").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	})
	$(".week").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	})
	$(".month").css({
		"background" : "green",
		"color" : "white"
	})
	$("#actionBtn").css({
		"display":"inline"
	})
}

</script>

</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">

		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td><img alt="" src="${ctxPath }/images/gray_left.png"
					class='left' id="home"></td>
				<td><img alt="" src="${ctxPath }/images/gray_right.png"
					class='right'></td>
			</Tr>
			<tr>
				<td><img alt="" src="${ctxPath }/images/inven_left.png"
					class='menu_left'></td>
				<td><img alt="" src="${ctxPath }/images/purple_right.png"
					class='menu_right'></td>
			</tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%">
						<tr>
							<td>
								작업자 : <input id="select_worker_combo" style="border-color: black;">
								<div class="rangeSelector" style="display: -webkit-inline-box;">
									<button id="datePicker" class="day" onclick="daily()">일간</button>					
									<button id="datePicker" class="week" onclick="weekly()">주간</button>					
									<button id="datePicker" class="month" onclick="monthly()">월간</button>
								</div>
								<div id="dailyRange" style="display: inline">
									<spring:message code="search_term"></spring:message>
									<input type="text" id="sDate_attandant"> ~ <input type="text" id="eDate_attandant">
								</div>
								<div id="actionBtn" style="display: inline; float: right; margin:auto;">
									<i class="k-icon k-i-search k-icon-64" onclick="getAttentList()"></i>
									<button id="print" onclick="pop_print()"><i class="fa fa-print" aria-hidden="true"></i> 프린트</button>
								</div>										
									<div id="attandant_grid"></div>
							</td>
						</tr>
					</table>
				</td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class="nav_span"></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
		</table>
	</div>
	<div id="print_table" style="display: none;"></div>
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>
