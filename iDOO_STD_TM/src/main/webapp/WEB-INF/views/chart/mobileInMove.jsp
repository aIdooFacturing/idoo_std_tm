<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>

<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>Insert title here</title>
</head>
<style>
body {
	margin: 0%;
	padding: 0%;
	width: 100%;
	height: 100%;
	position: absolute;
}

#titleName {
	width: 100%;
	height: 15%;
}

#section1 {
	width: 100%;
	height: 20%;

}

#section2 {
	width: 100%;
	height: 65%;
}
</style>
<body>
	<div id="titleName">
		입고
	</div>
	<div id="section1">
		처리 결과 알림
	</div>
	<div id="section2">
		처리 내용 알림
	</div>
</body>
</html>