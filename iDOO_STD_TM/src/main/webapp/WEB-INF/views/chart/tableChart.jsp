<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">
var shopId = 2;

function replaceHyphen(str){
	return str.replace(/-/gi,"#");
};

var panel = false;
$(function(){
	
	setEl();
	getData();
	

	$("#menu_btn").click(function(){
		if(!panel){
			showPanel();
		}else{
			closePanel();
		};
		panel = !panel;
	});
	$(".menu").click(goReport);
	
	
});


var shopId = 1;

function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function getData(){
	var url = ctxPath + "/chart/getFacilitiesStatus.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var table = "";
			$(json).each(function(idx, data){
				table += "<tr>" + 
							"<td>" + (idx+1) + "</td>" +
							"<td>" + data.cd + "</td>" + 
							"<td>" + data.name + "</td>" + 
							"<td>" + data.nameAbb + "</td>" + 
							"<td>" + data.WCCD + "</td>" + 
							"<td>" + decodeURIComponent(data.JIG) + "</td>" + 
							"<td>" + decodeURIComponent(data.isAuto) + "</td>" + 
							"<td>" + decodeURIComponent(data.type) + "</td>" + 
							"<td>" + data.EXCD + "</td>" + 
							"<td>" + data.NC + "</td>" +
							"<td>" + data.PRDPRGM + "</td>" +
							"<td>" + data.CNTMCD + "</td>" + 
							"<td>" + data.tgCnt + "</td>" +
							"<td>" + data.tgRunTime + "</td>" +
							"<td></td>" +
						"</tr>";
			});
			
			$("#table").append(table);
			$("#table td").css({
				"text-align" : "center",
				"color" : "white",
				"border" : getElSize(5) + "px solid white",
				"font-size" : getElSize(40),
				"padding" : getElSize(20)
			});
		}
	});
};

function setEl() {
	$(".container").css({
		"width": originWidth,
		"height" : originHeight,
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100),
		"color" : "white"
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
		"width" : contentWidth * 0.1
	});


	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(50)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});

	$("#panel_table td").addClass("unSelected_menu");

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	$("#wcList").css({
		"position" : "absolute",
		"left" : originWidth/2 + $("#wcList").width()/2,
		"top" : originHeight/2 - $("#wcList").height()/2,
		"z-index" : 9999999,
		"display" : "none"
	}); 
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(100),
		"left" : getElSize(20),
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});

	$("#panel_table td").addClass("unSelected_menu");
	
	$(".goGraph, .excel, .label, #dvcSelector, #table").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
		//"border": getElSize(5) + "px solid gray"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(40)
	});
	
	$(".chartTable").css({
		"margin-top" : getElSize(60),
		"font-size" : getElSize(50),
		//"padding" : getElSize(20),
		"border-collapse" : "collapse"
	});
	
	$(".chartTable td").css({
		"border" : "solid 1px gray"
	});
	
	
	$("#chart").css({
		"margin-bottom" : getElSize(20)
	});
	
	$("#arrow_left, #arrow_right").css({
		"width" : getElSize(100),
		"margin-left" : getElSize(50),
		"margin-right" : getElSize(50),
	});
	
	$("#table td").css({
		"color" : "white",
		"border" : getElSize(5) + "px solid white"
	});
};

function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};



function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/main3.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/singleChartStatus2.do";
		window.localStorage.setItem("dvcId", 1)
		location.href = url;
	}else if(type=="menu4"){
		url = "${ctxPath}/chart/tableChart.do";
		location.href = url;
	}
};
</script>
</head>

<body oncontextmenu="return false">
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title">(주) 부광정밀</div>	
	
	<div id="panel">
		<table id="panel_table" width="100%">
			<tr>
					<td id="menu0" class="menu">샵 레이아웃</td>
				</tr>
				<tr>
					<td id="menu9" class="menu">개별 장비 가동 현황</td>
				</tr>
				<tr>
					<td id="menu1" class="menu">장비별 가동실적 분석</td>
				</tr>
				<tr>
					<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
				</tr>
				<tr>
					<td id="menu3" class="menu">무인장비 가동 현황</td>
				</tr>
				<tr>
					<td id="menu4" class="menu">설비현황</td>
				</tr>
				<!-- <tr>
					<td id="menu4" class="menu">야간무인장비 가동 현황</td>
				</tr> -->
		</table>
	</div>
	<img src="${ctxPath }/images/menu.png" id="menu_btn" >
	
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
			<center>
				<div class="title">설비현황</div>
				
				<table id="table" style="width:95%; border-collapse: collapse;">
					<Tr align="center">
						<td>순번</td>
						<td>설비코드</td>
						<td>설비명</td>
						<td>설비명 약어</td>
						<td>WC코드</td>
						<td>보유직</td>
						<td>무인여부</td>
						<td>설비유형</td>
						<td>기존설비코드</td>
						<td>NC</td>
						<td>프로그램</td>
						<td>M코드(카운터최종)</td>
						<td>일일생산목표</td>
						<td>일일목표가동시간</td>
						<td>비고</td>
						
					</Tr>
				</table>
			</center>
		</div>
	</div>
	
</body>
</html>