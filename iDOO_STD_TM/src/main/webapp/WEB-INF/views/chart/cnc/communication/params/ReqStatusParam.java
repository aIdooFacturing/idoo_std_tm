package com.unomic.cnc.communication.params;

public class ReqStatusParam implements Params
{
	private int path;
	private int req_type;
	
	public ReqStatusParam(int path, int req_type)
	{
		this.path = path;
		this.req_type = req_type;
	}

	public int getPath()
	{
		return path;
	}

	public void setPath(int path)
	{
		this.path = path;
	}

	public int getReq_type()
	{
		return req_type;
	}

	public void setReq_type(int req_type)
	{
		this.req_type = req_type;
	}

	@Override
	public String resizeString()
	{
		return null;
	}

	@Override
	public int getMessageLength()
	{
		return 8;
	}
}
