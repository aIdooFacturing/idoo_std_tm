package com.unomic.cnc.communication.params;

import com.unomic.cnc.StringUtils;

public class ReqLoginParam implements Params
{
	private String account;			// id
	private String passwd;			// password
	private int device_type;		// 0: android, 1:iphone
	private String reg_token;		// C2DM registration id
	
	public String getAccount()
	{
		return account;
	}
	public void setAccount(String account)
	{
		account = StringUtils.resize(account, CMD_LOGIN_REQ_FIELD_ACCOUNT_SIZE);
		this.account = account;
	}
	public String getPasswd()
	{
		return passwd;
	}
	public void setPasswd(String passwd)
	{
		passwd = StringUtils.resize(passwd, CMD_LOGIN_REQ_FIELD_PASSWD_SIZE);
		this.passwd = passwd;
	}
	public int getDevice_type()
	{
		return device_type;
	}
	public void setDevice_type(int device_type)
	{
		this.device_type = device_type;
	}
	public String getReg_token()
	{
		return reg_token;
	}
	public void setReg_token(String reg_token)
	{
		reg_token = StringUtils.resize(reg_token, CMD_LOGIN_REQ_FIELD_REG_TOKEN_SIZE);
		this.reg_token = reg_token;
	}

	@Override
	public String resizeString()
	{
		return account + passwd + device_type + reg_token;
	}
	@Override
	public int getMessageLength()
	{
		return resizeString().getBytes().length;
	}
}
