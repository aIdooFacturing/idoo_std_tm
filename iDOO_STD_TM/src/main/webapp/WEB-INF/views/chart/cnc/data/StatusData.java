package com.unomic.cnc.data;

import java.util.ArrayList;
import java.util.List;


public class StatusData 
{
	public static String spindle;
	public static String spindle_value;
	
	public static List<String> spindles;
	public static List<String> spindles_values;
	
	public static List<Axis> position1;
	public static List<Axis> position2;
	public static List<Axis> position3;
	public static List<Axis> position4;
	public static List<Axis> position5;
	public static List<Axis> position6;
	public static List<Axis> position7;
	
	public static List<String> aixCoordinateTitle;
	public static List<String> timeTitleEn;
	public static List<String> timeTitleEnValue;
	public static List<String> partTitle;
	public static List<String> partValue;
	
	public static String machineProgramName;
	
	public static String status;
	public static String mode;
	public static boolean alarmExist = false;
	
	public static ArrayList<Axis> relativeAxisList;
	public static ArrayList<Axis> absoluteAxisList;
	public static ArrayList<Axis> machineAxisList;
	public static ArrayList<Axis> distAxisList;
	
	public static byte[][] programList;
	public static long caretLine;
	
	public static ArrayList<Alarm> alarmList;
	public static int alarmCount;
	public static String alarmNumber;
	
	public static short[] gModal;
	public static String[] gModals;
	
	public static short[] auxCode;
	public static String auxCodeTitle;
	
	public static float spindleLoad;
	public static double spindleActual;
	public static double spindleTarget;
	
	public static String feedActual;
	public static String feedTarget;
	
	public static String runTime = "";
	public static String cuttingTime = "";
	public static String timer = "";
	public static String cycleTime = "";
	
	public static long machinedPart;
	public static long totalMachinedPart;
	public static long requiredPart;
}
