<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	var sDate = "${sDate}";
	var eDate = "${eDate}";
	
	function getGroup(){
		//var url = "${ctxPath}/chart/getGroup.do";
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#group").html(option);
				
				getTableData("jig");
			}
		});
	};

	var jigCsv;
	var wcCsv;
	var selected_dvc;
	var className = "";
	var classFlag = true;
	var preLine;
	var sum_target_op_time = 0;
		sum_total_op_time = 0;
		sum_op_date = 0;
		sum_incycle = 0;
		sum_wait = 0;
		sum_alarm = 0;
		sum_noConn = 0;
		

	var dvc;

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};



	function getMachineTy(name){
		ty = "";
		if(decode(name).indexOf("내수")!=-1){
			ty = "내수 ";
		}else if(decode(name).indexOf("북미")!=-1){
			ty = "북미 ";
		}else if(decode(name).indexOf("유럽")!=-1){
			ty = "유럽 ";
		};
		
		name = ty + decode(name).replace(/\+/gi, " ").substr(decode(name).lastIndexOf("#")-1,1);
		return name;
	};
	
	var jigCsv;
	var wcCsv;
	var selected_dvc;
	var className = "";
	var classFlag = true;
	var preLine;
	var sum_target_op_time = 0;
		sum_total_op_time = 0;
		sum_op_date = 0;
		sum_incycle = 0;
		sum_wait = 0;
		sum_alarm = 0;
		sum_noConn = 0;
		
	function getTableData(el){
		classFlag = true;
		var sDate = $("#jig_sdate").val();
		var eDate = $("#jig_edate").val() + " 23:59:59";
		var checkedStandard='dvc';
		var url = "${ctxPath}/chart/getPerformanceAgainstGoal.do";

		window.localStorage.setItem("jig_sDate", sDate);
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate +
					"&shopId=" + shopId + 
					"&prdNo=" + $("#group").val() +
					"&checkedStandard=" +checkedStandard;
		console.log(url+"?"+param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				jigData = "${prdct_machine_line}, ${device}, Cycle Time (${minute}),Cycle Time (${second}), Cavity, UPH, Capa, ${plan}, ${machine_performance}, ${performance_insert}, ${faulty}, ${target_ratio}, ${faulty_ratio}, ${target}, ${incycle}, ${wait}, ${stop}, Power OFf, ${avg_cycle_time}, ${op_ratio}LINE "

				rowSpanArray = [];
				rowSpanArrayLine = [];
				var preName = json[0].group;
				var preLine = getMachineTy(json[0].name);
				var i = 0;
				var j = 0;
				
				var array = [];
				
				var sum_capa = 0;
				var sum_target = 0;
				var sum_cnt = 0;
				var sum_dvc_cnt = 0;
				var sum_fault = 0;
				var sum_targetRatio = 0;
				var sum_faultRatio = 0;
				var sum_target_time = 0;
				var sum_incycle_time = 0;
				var sum_wait_time = 0;
				var sum_alarm_time = 0;
				var sum_noConn_time = 0;
				var sum_AvrCycleTime = 0;
				var sum_opRatio = 0;
				var sum_partCyl = 0;
				
				var total_capa = 0;
				var total_target = 0;
				var total_cnt = 0;
				var total_dvc_cnt = 0;
				var total_fault = 0;
				var total_targetRatio = 0;
				var total_faultRatio = 0;
				var total_target_time = 0;
				var total_incycle_time = 0;
				var total_wait_time = 0;
				var total_alarm_time = 0;
				var total_noConn_time = 0;
				var total_AvrCycleTime = 0;
				var total_opRatio = 0;
				var total_partCyl = 0;
				
				$(json).each(function(idx, data){
					if(preName!=data.group){
						preName = data.group;
						rowSpanArray.push(i+1);
						i = 0;
						
						array = [j, preLine]
						rowSpanArrayLine.push(array);
						j = 0;
						preLine = "re";
					};
					i++;
					
					if(preLine!=getMachineTy(data.name)){
						if(j!=0){
							array = [j,preLine]
							rowSpanArrayLine.push(array);
							j = 0;	
						}
						preLine = getMachineTy(data.name);
					};
					j++;
				});
				
				sum = 0;
				for(var i = 0; i < rowSpanArray.length; i++){
					sum += rowSpanArray[i]-1;
				};
				
				rowSpanArray.push(json.length - sum+1);
				
				sum = 0;
				for(var i = 0; i < rowSpanArrayLine.length; i++){
					sum += rowSpanArrayLine[i][0];
				};
				
				array = [json.length - sum, getMachineTy(json[json.length-1].name)]
				rowSpanArrayLine.push(array);
				
				//$(".contentTr").remove();
				//var tr = "<tbody>";
				
				
					
				var target_ratio = "${target_ratio}";
				var tr = "<thead>" + 
						"<tr style='font-weight: bolder; background-color: rgb(34,34,34)' class='thead'>" + 
							"<Td rowspan='2'  width='5%'>${prdct_machine_line}</Td>" + 
							"<Td rowspan='2' width='7%' >${line}</Td>" + 
							"<Td rowspan='2' >${device}</Td>" + 
							"<td colspan='4' >${standard}</td>" + 
							"<td colspan='4'>${worker}</td>" + 
							"<td colspan='8'>${prdct_performance}</td>" + 
							"<td colspan='7'>${op_performance}</td>" + 
						"</tr>" + 
						"<tr style='font-weight: bolder; background-color: rgb(34,34,34)' class='thead'>" + 
							"<td  >Cycle<Br>Time<Br>(${minute})</td>" + 
							"<td  >Cycle<br>Time<br>(${second})</td>" +  
							"<td  >Cavity</td>" + 
							"<td  >UPH</td>" + 
							"<td  >${name}</td>" + 
							"<td  >${n_d}</td>" + 
							"<td  >${work}</td>" + 
							"<td  >${assiduity}</td>" + 
							"<td >Capa</td>" + 
							"<td >${plan}</td> " +
							"<td >${machine_performance}</td>" +
							"<td  width='3%'>HMI</td>" + 
							"<td >${performance_insert}</td>" + 
							"<td >${faulty}</td>" + 
							"<td >" + target_ratio.replace(/&nbsp;/gi,"") + "</td>" +  
							"<td >${faulty_ratio}</td>" + 
							"<td >${target}</td>" + 
							"<td >${incycle}</td>" + 
							"<td >${wait}</td>" + 
							"<td >${stop}</td>" + 
							"<td >Power<br>Off</td>" + 
							"<td >${avg_cycle_time}</td>" + 
							"<td >${op_ratio}</td>" + 
						"</tr>" + 
					"</thead>";
					
			//	jigData = "생산차종,장비,근무시간,생산 Capa (EA), 생산 계획 (EA),생산 실적(EA),달성율 (%)LINE";
				
				var preGroup = "";
				var preLine = "";
				i = 0;
				j = 0;
				var rowSpan = 1;
				var rowSpanLine = 1;
				
				var dvcCnt = 0;
				var totaldvcCnt = 0;
				var first = true;
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					var group = "";
					var total = "";
					
					if(preGroup!=data.group){
						rowSpan = rowSpanArray[i];

						preGroup = data.group;
						group = "<td width='5%' align='center' rowspan=" +(rowSpan*2-1) + " ;>" + data.group + "</td>";
						
						i++;

						rowSpanLine = rowSpanArrayLine[j][0];
						preLine = "re";
						line = "<td align='center' rowspan=" + (rowSpanLine*2) + " ;>" + rowSpanArrayLine[j][1] + "</td>";
						
					}else{
						group = "";
					};
					
					var line = "";
					if(preLine!=getMachineTy(data.name)){
						rowSpanLine = rowSpanArrayLine[j][0];
						
						preLine = getMachineTy(data.name);
						
						var lineName = rowSpanArrayLine[j][1];
						if(lineName.indexOf("M")!=-1){
							lineName = rowSpanArrayLine[j][1] + "CT " + rowSpanArrayLine[j][0] + "대";
						}else if(lineName.indexOf("C")!=-1){
							lineName = rowSpanArrayLine[j][1] + "NC " + rowSpanArrayLine[j][0] + "대";
						}else if(lineName.indexOf("R")!=-1){
							lineName = rowSpanArrayLine[j][1] + "삭 " + rowSpanArrayLine[j][0] + "대";
						}
						
						line = "<td width='7%' align='center' rowspan=" + (rowSpanLine*2) + " >" + lineName + "</td>";
						j++;
						
					}else{
						line = "";
					};
					
					
					if(!first && group!=""){
						tr += "<tr>" +
								"<Td colspan='10'>${sum}</td>" +
								"<Td>" + sum_capa + "</td>" +
								"<Td>" + sum_target + "</td>" +
								"<Td>" + sum_dvc_cnt + "</td>" +
								"<Td>" + sum_partCyl + "</td>" +
								"<Td>" + sum_cnt + "</td>" +
								"<Td>" + sum_fault + "</td>" +
								"<Td>" + Number(sum_targetRatio/dvcCnt).toFixed(1) + "%</td>" +
								"<Td>" + Number(sum_faultRatio/dvcCnt).toFixed(1) + "%</td>" +
								"<Td>" + Number(sum_target_time).toFixed(1) + "</td>" +
								"<Td>" + Number(sum_incycle_time).toFixed(1) + "</td>" +
								"<Td>" + Number(sum_wait_time).toFixed(1) + "</td>" +
								"<Td>" + Number(sum_alarm_time).toFixed(1) + "</td>" +
								"<Td>" + Number(sum_noConn_time).toFixed(1) + " </td>" +
								"<Td>" + Number(sum_AvrCycleTime).toFixed(1) + "</td>" +
								"<Td>" + Number(sum_opRatio/dvcCnt).toFixed(1) + "%</td>" +
							"</tr>";
							
							jigData += "소계 ," + 
										"," +
										"," + 
										"," + 
										"," + 
										"," + 
										sum_capa + ","+ 
										sum_target +","+ 
										sum_dvc_cnt +","+ 
										sum_cnt + ","+ 
										sum_fault + ","+ 
										Number(sum_targetRatio/dvcCnt).toFixed(1) + ","+ 
										Number(sum_faultRatio/dvcCnt).toFixed(1) + "%" + ","+ 
										Number(sum_target_time).toFixed(1) +","+  
										Number(sum_incycle_time).toFixed(1) +","+ 
										Number(sum_wait_time).toFixed(1) + ","+ 
										Number(sum_alarm_time).toFixed(1) +","+ 
										Number(sum_noConn_time).toFixed(1) + ","+ 
										Number(sum_AvrCycleTime).toFixed(1) + ","+ 
										Number(sum_opRatio/dvcCnt).toFixed(1) + "%LINE";
										
							
						sum_capa = 0;
						sum_target = 0;
						sum_cnt = 0;
						sum_dvc_cnt = 0;
						sum_fault = 0;
						sum_targetRatio = 0;
						sum_faultRatio = 0;
						sum_target_time = 0;
						sum_incycle_time = 0;
						sum_wait_time = 0;
						sum_alarm_time = 0;
						sum_noConn_time = 0;
						sum_AvrCycleTime = 0;
						sum_opRatio = 0;
						sum_partCyl = 0;
						
						dvcCnt = 0;
						
					};
					
					var faultRatio = Number(data.faultCnt / data.cntCyl * 100).toFixed(1);
					if(data.cntCyl==0) faultRatio = 0;
				
					sum_capa += Number(data.capa);
					sum_target += Number(data.tgCyl);
					sum_cnt += Number(data.cntCyl);
					sum_dvc_cnt += Number(data.dvcCntCyl);
					sum_fault += Number(data.faultCnt);
					sum_targetRatio += Number(data.targetRatio);
					sum_faultRatio += Number(faultRatio);
					sum_target_time += Number(data.target_time/3600);
					sum_incycle_time += Number(data.inCycle_time/3600);
					sum_wait_time += Number(data.wait_time/3600);
					sum_alarm_time += Number(data.alarm_time/3600);
					sum_noConn_time += Number(data.noConnTime/3600);
					sum_AvrCycleTime += Number(data.lastAvrCycleTime/60);
					sum_opRatio += Number(data.opRatio);
					sum_partCyl += Number(data.partCyl);
					
					total_capa += Number(data.capa);
					total_target += Number(data.tgCyl);
					total_cnt += Number(data.cntCyl);
					total_dvc_cnt += Number(data.dvcCntCyl);
					total_fault += Number(data.faultCnt);
					total_targetRatio += Number(data.targetRatio);
					total_faultRatio += Number(faultRatio);
					total_target_time += Number(data.target_time/3600);
					total_incycle_time += Number(data.inCycle_time/3600);
					total_wait_time += Number(data.wait_time/3600);
					total_alarm_time += Number(data.alarm_time/3600);
					total_noConn_time += Number(data.noConnTime/3600);
					total_AvrCycleTime += Number(data.lastAvrCycleTime/60);
					total_opRatio += Number(data.opRatio);
					total_partCyl += Number(data.partCyl);
					
					dvcCnt++;
					totaldvcCnt++;
					
					jigData += data.group.replace(/,/gi, " ") + "," + 
								decode(data.name).replace(/\+/gi, " ").substr(decode(data.name).lastIndexOf("#")-1) +","+  
								data.cycleTmM +","+ 
								data.cycleTmS + ","+ 
								data.cvt + ","+ 
								data.uph + ","+ 
								data.capa + ","+ 
								data.tgCyl +","+ 
								data.dvcCntCyl +","+
								data.partCyl +","+
								data.cntCyl + ","+ 
								data.faultCnt + ","+ 
								data.targetRatio + ","+ 
								faultRatio + "%" + ","+ 
								Number(data.target_time/3600).toFixed(1) +","+  
								Number(data.inCycle_time/3600).toFixed(1) +","+ 
								Number(data.wait_time/3600).toFixed(1) + ","+ 
								Number(data.alarm_time/3600).toFixed(1) +","+ 
								Number(data.noConnTime/3600).toFixed(1) + ","+ 
								Number(data.lastAvrCycleTime/60).toFixed(1) + ","+ 
								Number(data.opRatio).toFixed(1) + "%LINE";
						
					tr += "<tr class='contentTr " + className + "' >" +
								group + 
								line + 
								"<td rowspan='2' >" + decode(data.name).replace(/\+/gi, " ").substr(decode(data.name).lastIndexOf("#")-1) + "</td>" +
								"<Td rowspan='2' '>" + data.cycleTmM + "</td>" +
								"<Td rowspan='2' >" + data.cycleTmS + "</td>" +
								"<Td rowspan='2'>" + data.cvt + "</td>" + 
								"<Td rowspan='2' >" + data.uph +"</td>" + 
								"<Td >" + decodeURIComponent(data.workerD).replace(/\+/gi, " ") + "</td>" + 
								"<Td >${day}</td>" + 
								"<Td >" + data.workTmMD+"1111" + "</td>" + 
//								"<Td >" + data.nonOpTimeD + "</td>" + 
								"<Td >" + 0 + "</td>" + 
								"<Td rowspan='2'>" + data.capa + "</td>" + 
								"<Td rowspan='2'>" + data.tgCyl + "</td>" +
								"<Td rowspan='2'>" + data.dvcCntCyl + "</td>" + 
								"<Td rowspan='2' width='3%'>" + data.partCyl + "</td>" +
								"<Td rowspan='2'>" + data.cntCyl + "</td>" +
								"<Td rowspan='2'>" + data.faultCnt + "</td>" + 
								"<Td rowspan='2'>" + data.targetRatio + "%</td>" + 
								"<Td rowspan='2'>" + faultRatio + "%</td>" + 
								"<Td rowspan='2'>" + Number(data.target_time/3600).toFixed(1) + "</td>" + 
								"<Td rowspan='2' >" + Number(data.inCycle_time/3600).toFixed(1) + "</td>" + 
								"<Td rowspan='2' >" + Number(data.wait_time/3600).toFixed(1) + "</td>" + 
								"<Td rowspan='2' >" + Number(data.alarm_time/3600).toFixed(1) + "</td>" + 
								"<Td rowspan='2' >" + Number(data.noConnTime/3600).toFixed(1) + "</td>" +  
								"<Td rowspan='2' >" + Number(data.lastAvrCycleTime/60).toFixed(1) + "</td>" +
								"<Td rowspan='2' >" + Number(data.opRatio).toFixed(1) + "%</td>" + 
						 "</tr>" + 
						 "<tr class='contentTr " + className + "' >" + 
						 	"<td  '>" + decodeURIComponent(data.workerN).replace(/\+/gi, " ") + "</td>" +
						 	"<td  '>${night}</td>" +
						 	"<td  '>" + data.workTmMN + "</td>" +
//						 	"<td '>" + data.nonOpTimeN + "</td>" + 
						 	"<td '>" + 0 + "</td>" + 
						 "</tr>";
						 
				
				first = false;

				});
				
				
				jigData += "소계," + 
							"," +
							"," + 
							"," + 
							"," + 
							"," + 
							sum_capa + ","+ 
							sum_target +","+ 
							sum_dvc_cnt +","+
							sum_partCyl +","+
							sum_cnt + ","+ 
							sum_fault + ","+ 
							Number(sum_targetRatio/dvcCnt).toFixed(1) + ","+ 
							Number(sum_faultRatio/dvcCnt).toFixed(1) + "%" + ","+ 
							Number(sum_target_time).toFixed(1) +","+  
							Number(sum_incycle_time).toFixed(1) +","+ 
							Number(sum_wait_time).toFixed(1) + ","+ 
							Number(sum_alarm_time).toFixed(1) +","+ 
							Number(sum_noConn_time).toFixed(1) + ","+ 
							Number(sum_AvrCycleTime).toFixed(1) + ","+ 
							Number(sum_opRatio/dvcCnt).toFixed(1) + "%LINE";
							
				tr += "<tr>" +
						"<Td colspan='10'>${sum}</td>" +
						"<Td>" + sum_capa + "</td>" +
						"<Td>" + sum_target + "</td>" +
						"<Td>" + sum_dvc_cnt + "</td>" +
						"<Td>" + sum_partCyl + "</td>" +
						"<Td>" + sum_cnt + "</td>" +
						"<Td>" + sum_fault + "</td>" +
						"<Td>" + Number(sum_targetRatio/dvcCnt).toFixed(1) + "%</td>" +
						"<Td>" + Number(sum_faultRatio/dvcCnt).toFixed(1) + "%</td>" +
						"<Td>" + Number(sum_target_time).toFixed(1) + "</td>" +
						"<Td>" + Number(sum_incycle_time).toFixed(1) + "</td>" +
						"<Td>" + Number(sum_wait_time).toFixed(1) + "</td>" +
						"<Td>" + Number(sum_alarm_time).toFixed(1) + "</td>" +
						"<Td>" + Number(sum_noConn_time).toFixed(1) + " </td>" +
						"<Td>" + Number(sum_AvrCycleTime).toFixed(1) + "</td>" +
						"<Td>" + Number(sum_opRatio/dvcCnt).toFixed(1) + "%</td>" +
					"</tr>" +  
					"<tr>" +
						"<Td colspan='11'>${total}</td>" +
						"<Td>" + total_capa + "</td>" +
						"<Td>" + total_target + "</td>" +
						"<Td>" + total_dvc_cnt + "</td>" +
						"<Td>" + total_partCyl + "</td>" +
						"<Td>" + total_cnt + "</td>" +
						"<Td>" + total_fault + "</td>" +
						"<Td>" + Number(total_targetRatio/totaldvcCnt).toFixed(1) + "%</td>" +
						"<Td>" + Number(total_faultRatio/totaldvcCnt).toFixed(1) + "%</td>" +
						"<Td>" + Number(total_target_time).toFixed(1) + "</td>" +
						"<Td>" + Number(total_incycle_time).toFixed(1) + "</td>" +
						"<Td>" + Number(total_wait_time).toFixed(1) + "</td>" +
						"<Td>" + Number(total_alarm_time).toFixed(1) + "</td>" +
						"<Td>" + Number(total_noConn_time).toFixed(1) + "</td>" +
						"<Td>" + Number(total_AvrCycleTime).toFixed(1) + "</td>" +
						"<Td>" + Number(total_opRatio/totaldvcCnt).toFixed(1) + "%</td>" +
					"</tr>" +
					"</tbody>"; 
				
					
					jigData += "전체," + 
								"," +
								"," + 
								"," + 
								"," + 
								"," + 
								total_capa + ","+ 
								total_target +","+ 
								total_dvc_cnt +","+ 
								total_cnt + ","+ 
								total_fault + ","+ 
								Number(total_targetRatio/totaldvcCnt).toFixed(1) + ","+ 
								Number(total_faultRatio/totaldvcCnt).toFixed(1) + "%" + ","+ 
								Number(total_target_time).toFixed(1) +","+  
								Number(total_incycle_time).toFixed(1) +","+ 
								Number(total_wait_time).toFixed(1) + ","+ 
								Number(total_alarm_time).toFixed(1) +","+ 
								Number(total_noConn_time).toFixed(1) + ","+ 
								Number(total_AvrCycleTime).toFixed(1)  + ","+ 
								Number(total_opRatio/totaldvcCnt).toFixed(1) + "%LINE";
								
					
				$(".tmpTable").html(tr);
			 	$(".tmpTable td").css({
					"border" : getElSize(2) + "px solid black",
					"font-size" : getElSize(35)	
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222",
					"font-size" : getElSize(35)
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232",
					"font-size" : getElSize(35)
				}); 
				
				//setEl();
				
				$(".tableContainer div:last").remove()
				scrolify($('.tmpTable'), getElSize(1380));
				
				$("*").not(".container, #dvcDiv").css({
					"overflow-x" : "hidden",
					//"overflow-y" : "auto"
				});
				
				$("html").css("overflow-y","hidden");
			}
		});
	};

	var dvc;

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};


	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		//$("#jig_sdate").val(caldate(7));
		$("#jig_sdate").val(sDate);
		$("#jig_edate").val(eDate);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
		
	
	var handle = 0;
	$(function(){
		createNav("analysis_nav",0);
		getGroup();
		setDate();	
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		setEl();
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			//"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".left,.right").css({
			"height" : getElSize(120)
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_left").css({
			"width" : $(".left").width()
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").not("#intro").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr, .contentTr2").css({
			"font-size" : getElSize(40)
		});
		
		$(".tableContainer").css({
			"height" : getElSize(1620)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		location.href = "${ctxPath}/chart/performanceAgainstGoal_chart.do?sDate=" + $("#jig_sdate").val() + "&eDate=" + $("#jig_edate").val();
	};
	
	var jigData;
	function csvSend(){
		var id = this.id;
		var sDate, eDate;
		var csvOutput;
		
		jigData = jigData.replace(/&nbsp;/gi,"")
		jigData = jigData.replace(/<br>/gi,"")
		
		sDate = $("#jig_sdate").val();
		eDate = $("#jig_edate").val();
		csvOutput = jigData;
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/analysis_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left nav'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td><select id="group"></select>
								<spring:message code="op_period"></spring:message> <input type="date" class="date" id="jig_sdate"> ~ <input type="date" class="date" id="jig_edate"> 
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getTableData()">
								<button onclick="goGraph()" id="graph"><spring:message code="graph"></spring:message> </button>
								<button onclick="csvSend()" ><spring:message code="excel"></spring:message> </button>
							</td>
						</Tr>		
						<tr>
							<td>
								<div class="tableContainer" >
									<table style="color: white; width: 100%; text-align: center; border-collapse: collapse; " class="tmpTable" id="table2">
									</table>
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/selected_green.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back" style="display: none"></div>
	<span id="intro"></span>
</body>
</html>	